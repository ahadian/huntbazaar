## How to run this Technical test
## By Ahadian Akbar

1. type (cli)> cd backend & composer install
2. type (cli)> cp .env.example .env and config .env file
```
APP_NAME=Lumen
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE={your_database}
DB_USERNAME={username}
DB_PASSWORD={password}

CACHE_DRIVER=file
QUEUE_CONNECTION=sync
```
3. type (cli)> php artisan key:generate
4. type (cli)> php artisan migrate 
5. type (cli)> php db:seed
6. type (cli)> php db:seed --class=StatusAttendanceSeeder
7. type (cli)> php db:seed --class=DesignersSeeder
8. php artisan queue:table and add this command on .env file
```
QUEUE_CONNECTION=database
```
9. and then you should add these commands on .env file, for setup email integration purpose
```
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME={{your username}}
MAIL_PASSWORD={{your password}}
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS={{your email address}}
MAIL_FROM_NAME="${APP_NAME}"
```
10. php artisan config:cache
11. php artisan serve 
12. open your browser > http://localhost:8000 login with (u: admin@gmail.com, p:admin123)
13. php artisan queue:listen (for checking queue)
14. php artisan schedule:run (for checking the cron)

Here is my video documentation
https://www.loom.com/share/1e72cce4e291467c96325c0248f8d249







