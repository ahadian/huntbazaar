<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitations;
use App\Events;
use DataTables;
use App\Http\Requests\ForminvitationRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use URL;
class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $eventId = $request->event_id;
            $data = Invitations::latest()->where('event_id',$eventId)
            ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    if($row->status_id == 1) {
                       return 'New'; 
                    }
                    else if($row->status_id == 2)   {
                        return 'Form has been filled'; 
                    }
                    else{
                        return 'Thank you email has been sent';
                    }
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm editInvitation">Edit</a>';
                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-danger btn-sm deleteInvitation">Delete</a>';
                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Delete" class="btn btn-warning btn-sm resendInvitation">Resend Invitation</a>';
                    return $btn;
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }

    }

    /**
     * Resend email.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function resend(Request $request)
    {
        $invitation = Invitations::find($request->id);
        $details['email'] = $invitation->email;
        $details['url']   = URL::to('/forminvitation/') . "/" . $invitation->link;
        $job = (new \App\Jobs\SendEmailJob($details));
        dispatch($job);
        echo "sukses";
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ForminvitationRequest $request)
    {

        $setBlankData = mktime(0, 0, 0, 0, 0, 0);
        $Events = Events::find($request->eventId);

        $email = $request->email;
        $invitations = Invitations::where('id', $request->invitation_id)->first();
        $link = md5(time() . $email);
        if ($invitations !== null) {
            $invitations->update([
                'event_id'   => $request->eventId,
                'email'      => $email,
                'link'       => $link,
                'expiredAt'  => $Events->expiredAt,
                'filledAt'   => date('Y-m-d H:i:s', $setBlankData),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            Invitations::firstOrCreate([
                'event_id'   => $request->eventId,
                'email'      => $email,
                'link'       => $link,
                'status_id'  => 1,
                'expiredAt'  => $Events->expiredAt,
                'filledAt'   => date('Y-m-d H:i:s', $setBlankData),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

        $details['email'] = $email;
        $details['url']   = URL::to('/forminvitation/')."/".$link;
        $job = (new \App\Jobs\SendEmailJob($details));
        dispatch($job);
        // Mail::to($details['email'])->send(new \App\Mail\SendEmail($details));
        return response()->json(['success' => 'Invitations saved successfully!']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invitations  $Invitations
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Invitation = Invitations::find($id);
        return response()->json($Invitation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invitations  $Invitations
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Invitations::find($id)->delete();
        return response()->json(['success' => 'Invitations deleted!']);
    }
}
