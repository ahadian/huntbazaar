<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendances;
use App\Invitations;
use App\Designer;
use App\Event;
use DataTables;
use App\Http\Requests\FormattendanceRequest;

class AttendanceController extends Controller
{
    public function index($id)
    {
        $designer = Designer::all();
        $invitation = Invitations::where('link', $id)->first();

        if($invitation != null) {
            $now = strtotime(date('Y-m-d H:i:s'));

            $invitationMaxDate = strtotime($invitation->expiredAt);
            if($invitationMaxDate < $now) {
                return abort(403, 'This page has been expired'); 
            }

            if($invitation->status_id > 1) {
                return abort(403, 'This page has been filled'); 
            }
        }

        return view('invitationform',['data' => $invitation,'designer' => $designer]);
    }

    public function store(FormattendanceRequest $request)
    {

        $invitation = Invitations::where('id',$request->id)->first();

        if ($invitation !== null) {
            $invitation->update([
                'status_id'  => 2,
                'filledAt'   => date('Y-m-d H:i:s'),
            ]);
            Attendances::firstOrCreate([
                'nama'               => $request->name,
                'invitation_id'      => $invitation->id,
                'email'              => $request->email,
                'tanggal_lahir'      => date('Y-m-d H:i:s', strtotime($request->birthdate)),
                'jenis_kelamin'      => $request->gender,
                'designer_favorite'  => implode(",", $request->designer_favorite),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

        return response()->json(['success' => 'Invitations saved successfully!']);
    }
}
