<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;
use App\Http\Requests\FormloginRequest;

class AuthController extends Controller
{
    public function showFormLogin()
    {
        if (Auth::check()) {
            return redirect()->route('event.index');
        }
        return view('login');
    }

    public function login(FormloginRequest $request)
    {

        $data = [
            'email'     => $request->input('email'),
            'password'  => $request->input('password'),
        ];

        $userId = Auth::attempt($data);


        if (Auth::check()) {
            return redirect()->route('event.index');
        } else {
            Session::flash('error', 'Wrong email or password');
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }


}
