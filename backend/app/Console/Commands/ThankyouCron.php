<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Invitations;
use Illuminate\Support\Facades\Mail;
use URL;
class ThankyouCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thankyou:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        \Log::info("Cron is working fine!");
        $invitation = Invitations::where('status_id', 2)->all();

        foreach ($invitation as $key => $value) {
            $details['email'] = $value->email;
            $job = (new \App\Jobs\SendEmailJobThankyou($details));
            dispatch($job);
            Invitations::updateOrCreate(
                ['id'   => $value->id],
                ['status_id'  => 3]
            );
        }

        // return 0;
    }
}
