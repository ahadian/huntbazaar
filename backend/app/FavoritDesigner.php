<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoritDesigner extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'favorit_designer';
}
