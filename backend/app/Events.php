<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{

    protected $fillable = ['nama','expiredAt'];



    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';
}
