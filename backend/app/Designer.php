<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'designers';
}
