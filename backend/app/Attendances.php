<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendances extends Model
{
    protected $fillable = ['nama','email','invitation_id','jenis_kelamin','tanggal_lahir', 'designer_favorite'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attendances';
}
