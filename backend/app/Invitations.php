<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitations extends Model
{
    protected $fillable = ['event_id','email','link','status_id','expiredAt','filledAt'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Invitations';
}
