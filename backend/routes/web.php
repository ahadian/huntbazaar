<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\AttendanceController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('forminvitation/{id}', [AttendanceController::class, 'index'])->name('attendance.index');
Route::post('forminvitation/save/{id}', [AttendanceController::class, 'store'])->name('attendance.store');

Route::middleware(['auth'])->group(function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::resource('event', 'EventController');
        Route::resource('invitation', 'InvitationController');
        Route::get('resend', [InvitationController::class, 'resend'])->name('resend');
        Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    });
});


