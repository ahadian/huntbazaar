<?php

use Illuminate\Database\Seeder;

class StatusAttendanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('status_attendances')->insert([
            'id'    	 => 1,
            'nama'    	 => 'New',
            'created_at' => date('Y-m-d H:i:s')
        ]);

 
        DB::table('status_attendances')->insert([
            'id'    	 => 2,
            'nama'    	 => 'Form has been filled',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('status_attendances')->insert([
            'id'    	 => 3,
            'nama'    	 => 'Thank you email has been sent',
            'created_at' => date('Y-m-d H:i:s')
        ]);

    }
}
