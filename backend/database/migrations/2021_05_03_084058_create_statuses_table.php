<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_attendances', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->timestamps();
        });


        Schema::table('invitations', function (Blueprint $table) {
            $table->foreignId('status_id')->after('filledAt')
                  ->constrained('status_attendances')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_attendances');
    }
}
