
@extends('template')
@section('content')
<div class="section-header">
    <h1>Table</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Home</a></div>
        <div class="breadcrumb-item"><a href="#">Events </a></div>
    </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Events Data</h2>
        <p class="section-lead">Create event</p>

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                <h4>Events</h4>
                </div>
                <div class="card-body">
                    <a class="btn btn-success float-right" href="javascript:void(0)" id="createEvent"> Create New Event</a>
                    <div class="table-responsive">
                        <table class="table table-bordered table-md data-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Event Name</th>
                                    <th><strong>(Max date for fill invitation form)</strong></th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            </div>
        </div>



    </div>

    <style type="text/css">
        .pull-left{
            float: left !important;
        }
    </style>
    <script type="text/javascript">
  $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'nama', name: 'nama', searchable: true},
            {data: 'expiredAt', name: 'expiredAt'},
            {data: 'created_at', name: 'Created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('#createEvent').click(function () {
        $("#alertFail").hide();
        $("#alertFail").empty();
        $('#saveBtn').val("create-Event");
        $('#event_id').val('');
        $('#EventForm').trigger("reset");
        $('#modelHeading').html("Create New Event"); 
        $("#lastExpired").empty(); 
        $('#ajaxModel').modal('show');
    });

    $('body').on('click', '.editEvent', function () {

            $("#saveBtn").html('Save Changes');
            var event_id = $(this).data('id');
            $("#alertFail").hide();
            $("#alertFail").empty();
            $.get("event" +'/' + event_id +'/edit', function (data) {
            $('#modelHeading').html("Edit Event");
            $('#saveBtn').val("edit-user");
            $('#event_id').val(data.id);
            $('#name').val(data.nama);
            $("#lastExpired").html("<br /> last update at : " + data.expiredAt);

            $('#detail').val(data.detail);
        });
        $('#ajaxModel').modal('show');

   });

    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $("#lastExpired").empty(); 
        $(this).html('Sending..');
        $("#alertFail").hide();
        $("#alertFail").empty();
        $.ajax({
          data: $('#EventForm').serialize(),
          url: "",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#EventForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();
          },
          error: function (data) {
              $("#alertFail").show();
              var response = JSON.parse(data.responseText);
              var errorString = '<ul>';
              $.each( response.errors, function( key, value) {
                errorString += '<li>' + value + '</li>';
              });
              errorString += '</ul>'
              $("#alertFail").html(errorString);
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    $('body').on('click', '.deleteEvent', function () {

        var id = $(this).data("id");
        confirm("Are You sure want to delete !");

        $.ajax({
            type: "DELETE",
            url: "event"+'/'+id,
            data: {
                "_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('body').on('click', '.invitationEvent', function () {
        var id = $(this).data("id");
        $('#ajaxInvitations').modal('show');
        $("#eventId").val(id);
        $(".data-tables").dataTable().fnDestroy();

        var tables = $('.data-tables').DataTable({
            processing: true,
            serverSide: true,
            ajax: "invitation?event_id="+id,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'email', name: 'email'},
                {data: 'status', name: 'status'},
                {data: 'expiredAt', name: 'expiredAt'},
                {data: 'filledAt', name: 'filledAt'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });


    $('body').on('click', '.editInvitation', function () {
        $("#saveInvitations").html('Save Changes');
        var invitation_id = $(this).data('id');
        $("#alertFails").hide();
        $("#alertFails").empty();
        $.get("invitation" +'/' + invitation_id +'/edit', function (data) {
            $('#saveInvitations').val("edit-user");
            $('#invitation_id').val(data.id);
            $('#eventId').val(data.event_id);
            $('#email').val(data.email);
        });
   });

    $('body').on('click', '.resendInvitation', function () {
        var invitation_id = $(this).data('id');
        $.get("resend" +'/?id='+invitation_id, function (data) {
            alert('Resend invitation has been process')
        });
   });



     $('body').on('click', '.deleteInvitation', function () {

        var id = $(this).data("id");
        confirm("Are You sure want to delete !");

        $.ajax({
            type: "DELETE",
            url: "invitation"+'/'+id,
            data: {
                "_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                $('.data-tables').DataTable().draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    $('#saveInvitation').click(function (e) {
        e.preventDefault();
        $("#alertFails").hide();
        $("#alertFails").empty();

        $(this).html('Sending..');
        $.ajax({
          data: $('#InvitationForm').serialize(),
          url: "invitation",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#InvitationForm').trigger("reset");
              //$('#ajaxInvitations').modal('hide');
              $('.data-tables').DataTable().draw();
              $('#saveInvitation').html('Save Changes');

          },
          error: function (data) {
              $("#alertFails").show();
              var response = JSON.parse(data.responseText);
              var errorString = '<ul>';
              $.each( response.errors, function( key, value) {
                errorString += '<li>' + value + '</li>';
              });
              errorString += '</ul>'
              $("#alertFails").html(errorString);
              console.log('Error:', data);
              $('#saveInvitation').html('Save Changes');
          }
      });
    });

  });
   
</script>
@endsection

@section('divmodal')

<style type="text/css">
    .modal-lg {
        max-width: 80%;
    }
</style>
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
            <div class="modal-body">

                <div class="alert alert-danger alert-dismissible fade show" id="alertFail" style="display:none;" role="alert">

                </div>
                <form id="EventForm" name="EventForm" class="form-horizontal">
                    @csrf
                
                    <input type="hidden" name="event_id" id="event_id">
                    <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Event name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Event Name" value="" maxlength="50" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Max date for fill invitation form <strong id="lastExpired"></strong></label>
                        <div class="col-sm-12">
                            <input type="date" id="expiredAt" name="expiredAt" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">
                        Save changes
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ajaxInvitations" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Invitation Lists</h4>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
            <div class="modal-body">

                    <div class="alert alert-danger alert-dismissible fade show" id="alertFails" style="display:none;" role="alert">

                    </div>


                    <form id="InvitationForm" name="InvitationForm" class="form-horizontal">
                        @csrf
                        <input type="hidden" name="invitation_id" id="invitation_id">
                        <input type="hidden" name="eventId" id="eventId">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="email" name="email" 
                                placeholder="Enter Invitation Email" aria-label="">
                                <div class="input-group-append">
                                <button class="btn btn-primary" type="button" id="saveInvitation">Save changes</button>
                                </div>
                            </div>
                        </div>
                     
                    </form>
                    <div class="table-responsive">
                        <table class="table table-bordered table-md data-tables">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Expired At</th>
                                    <th>Filled At</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>


            </div>
        </div>
    </div>
</div>

    
@endsection
