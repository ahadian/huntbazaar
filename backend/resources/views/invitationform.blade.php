<lhtml>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('assets/invitationform.css') }}" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <title>Invitation form</title>

    <style type="text/css">
        li {
            display: inline-block;
            font-size: 1.2em;
            list-style-type: none;
            padding: 1em;
            text-transform: uppercase;
        }

        li span {
            display: block;
            font-size: 3.5rem;
        }
        @media all and (max-width: 768px) {
            h1 {
                font-size: 1.5rem;
            }
            
            li {
                font-size: 1.1rem;
                padding: .75rem;
            }
            
            li span {
                font-size: 3rem;
            }
        }
    </style>
  </head>
  <body>
    <div class="ctrlqFormContentWrapper">
      <div class="ctrlqHeaderMast"></div>
      <div class="ctrlqCenteredContent">
        <div class="ctrlqFormCard">
          <div class="ctrlqAccent"></div>
          <div class="ctrlqFormContent">
             
              <form id="invitationForm">
                <div class="row">
                     <div class="input-field col s12">
                         <div id="countdown">
                             <ul>
                             <li><span id="days"></span>days</li>
                             <li><span id="hours"></span>Hours</li>
                             <li><span id="minutes"></span>Minutes</li>
                             <li><span id="seconds"></span>Seconds</li>
                             </ul>
                         </div>
                        <h4 id="headline">Countdown</h4>

                     </div>
                </div>

              <div class="row">
                <div class="input-field col s12">
                  <h4>Form Invitation</h4>
                  <p>All fields are required</p>
                  <span id="alertData" style="display:none;color:red;">

                  </span>
                </div>
              </div>

              @csrf
              <div class="row">
                <div class="input-field col s12">
                  <input id="email" name="email" type="text" class="validate" data-error="#e2" required>
                  <label for="email">Email</label>
                  <div id="e2"></div>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <input id="name" name="name" type="text" class="validate" data-error="#e1" required>
                  <label for="name">Name</label>
                  <div id="e1"></div>
                </div>
              </div>

                 <div class="row">
                <div class="input-field col s12">
                  <label for="birthdate">Birth date</label>
                  <input type="date" id="birthdate" class="datepicker" name="birthdate" data-error="#e5" required>
                  <div id="e5"></div>
                </div>
              </div>



              <div class="row">
                <div class="input-field col s12">
                  <select id="gender" name="gender" class="validate" data-error="#e3" required>
                    <option value="" disabled selected>Gender</option>
                    <option value="L">Male</option>
                    <option value="P">Female</option>
                  </select>
                  <div id="e3"></div>
                </div>
              </div>


              <div class="input-field col s12">
                    <select multiple name="designer_favorite[]" id="designer_favorite[]">
                        <option value="" disabled selected>Favorit Design</option>
                        @foreach($designer as $key => $val)
                            <option value="{{ $val->id }}">{{ $val->nama }}</option>
                        @endforeach
                        
                    </select>
                    <label>Materialize Multiple Select</label>
              </div>

              <div class="row">
                <div class="input-field col m6 s12">
                  <button type="submit" id="submitBtn" class="waves-effect waves-light btn-large">
                    <i class="material-icons right">backup</i>Submit</button>
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>

    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/additional-methods.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('select').material_select();
            $('.datepicker').pickadate({
                selectMonths: true, 
                selectYears: 15
            });
            $.validator.setDefaults({
                ignore: []
            });
            

             $('#submitBtn').click(function (e) {
              e.preventDefault();
              $("#alertData").hide();
              $("#alertData").empty();
               $.ajax({
                data: $('#invitationForm').serialize(),
                url: "/forminvitation/save/{{$data->id}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                  alert('form has been sent');
                  location.reload();
                },
                error: function (data) {
                    var response = JSON.parse(data.responseText);
                    var errorString = '<ul>';
                    $.each( response.errors, function( key, value) {
                      errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul>'

                    $("#alertData").show();
                    $("#alertData").html(errorString);
                 
                }
            });
             });

            




            (function () {
            const second = 1000,
                    minute = second * 60,
                    hour = minute * 60,
                    day = hour * 24;

            //let birthday = "Sep 30, 2021 00:00:00",
            let birthday = "{{ $data->expiredAt }}",
                countDown = new Date(birthday).getTime(),
                x = setInterval(function() {    

                    let now = new Date().getTime(),
                        distance = countDown - now;

                    document.getElementById("days").innerText = Math.floor(distance / (day)),
                    document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                    document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                    document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

                    if (distance < 0) {
                    let headline = document.getElementById("headline"),
                        countdown = document.getElementById("countdown"),
                        content = document.getElementById("content");

                    headline.innerText = "";
                    countdown.style.display = "none";
                    content.style.display = "block";

                    clearInterval(x);
                    }
                }, 0)
            }());

        });

    </script>
  </body>

  </html>
